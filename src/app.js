const express = require('express');
const path = require('path');
const http = require('http');
const config = require('./config');
const bodyParser = require('body-parser');
const log = require('./helpers/logger')(module);

const users = require('./routers/users.route');

const app = express();

// Set middleware

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// Routes

app.use('/users', users);

// 404
app.use((req, res) => {
  res.status(404).send({
    message: 'Page not found',
  });
});

// Express error handling
app.use((err, req, res) => {
  res.status(500).json({
    message: err.message,
  });
});

// Node.js error handling
process.on('unhandledRejection', (err) => {
  if (err) {
    log.error(err);
  }
});

process.on('rejectionHandled', (err) => {
  if (err) {
    log.error(err);
  }
});

process.on('uncaughtException', (err) => {
  if (err) {
    log.error(err);
  }
});

app.listen(config.get('port'), config.get('host'), () => {
  log.info(`Server start on ${http.globalAgent.protocol}//${config.get('host')}:${config.get('port')}`);
});

module.exports = app;
