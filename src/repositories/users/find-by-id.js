const User = require('../../models/user');
const log = require('../../helpers/logger')(module);

module.exports = async (id, fields) => {
  try {
    const currentFields = fields || 'firstName,lastName,DOB,mainPhoto,education,aboutSelf,type';
    const fieldsFilter = {
      _id: id,
    };
    const fieldsProjection = currentFields.split(',').reduce((prev, current) => ({ ...prev, [current]: 1 }), {});
    const user = await User.findOne(fieldsFilter, fieldsProjection);

    return user;
  } catch (err) {
    log.error(err);
    throw err;
  }
};
