const log = require('../../helpers/logger')(module);
const ResponseJson = require('../../helpers/responseJson');
const userRepositories = require('../../repositories/users');
const descriptionConst = require('../constants/description');
const errorCodeConst = require('../constants/errorCode');

module.exports = async (req, res, next) => {
  try {
    const rj = new ResponseJson();
    const { userId } = req.params;
    const reqFields = req.query.fields;
    const user = await userRepositories.findById(userId, reqFields);
    if (!user) {
      res.setAll(404, descriptionConst.USER_NOT_FOUND, errorCodeConst.USER_NOT_FOUND);
      return res.status(rj.status).json(rj);
    }
    rj.set('user', user);
    rj.setDescription(descriptionConst.USER_WAS_GOT);
    return res.status(rj.status).json(rj);
  } catch (err) {
    log.error(err);
    return next(500);
  }
};
